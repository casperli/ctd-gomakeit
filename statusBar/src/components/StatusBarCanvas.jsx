import React, { Component, createElement } from "react";

export class StatusBarCanvas extends Component {
    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
        this.state = {
            canvasWidth: 2000,
            canvasHeight: 100
        }
    }

    componentDidMount() {
        this.initCanvas();
    }

    componentDidUpdate() {
        if (this.props.draft.status === "available" && this.props.pending.status === "available" && this.props.approved.status === "available") {
            const canvasContext = this.canvasRef.current.getContext("2d");
            this.generateBars(canvasContext);
        }
    }

    initCanvas() {  
        const canvasContext = this.canvasRef.current.getContext("2d");
        canvasContext.fillStyle = "white";
        canvasContext.fillRect(0, 0, this.canvasRef.current.width, this.canvasRef.current.height);
    }

    generateBars(ctx) {
        const draftValue = parseInt(this.props.draft.displayValue, 10) > 0 ? parseInt(this.props.draft.displayValue, 10) : 1;
        const pendingValue = parseInt(this.props.pending.displayValue, 10) > 0 ? parseInt(this.props.pending.displayValue, 10) : 1;
        const approvedValue = parseInt(this.props.approved.displayValue, 10) > 0 ? parseInt(this.props.approved.displayValue, 10) : 1;

        const areaPerValue = this.calculateAreaPerValue(draftValue, pendingValue, approvedValue);
        const areaPerDraft = draftValue * areaPerValue;
        const areaPerPending = pendingValue * areaPerValue;
        const areaPerApproved = approvedValue * areaPerValue;

        if (areaPerDraft > 0) {
            this.drawBar(ctx, 0, areaPerDraft, this.props.draftColor, "Drafts", this.props.draft.displayValue, this.props.draftTextColor);
        }
        if (areaPerPending > 0) {
            this.drawBar(ctx, areaPerDraft, areaPerPending, this.props.pendingColor, "Pending", this.props.pending.displayValue, this.props.pendingTextColor);
        }
        if (areaPerApproved > 0) {
            this.drawBar(ctx, areaPerDraft + areaPerPending, areaPerApproved, this.props.approvedColor, "Approved", this.props.approved.displayValue, this.props.approvedTextColor);
        }
    }

    drawBar(ctx, xStart, xEnd, color, text, value, textColor)  {
        ctx.fillStyle = color;
        ctx.fillRect(xStart, 0, xEnd, this.state.canvasHeight);
        ctx.font = "normal 25px Open Sans";
        ctx.fillStyle = textColor;
        ctx.fillText(text, (xEnd /2) + xStart - (ctx.measureText(text).width /2), 40);
        ctx.fillText(String(value), (xEnd /2) - 10 + xStart, 75);
    }

    calculateAreaPerValue(draftValue, pendingValue, approvedValue) {
        return this.state.canvasWidth / (draftValue + pendingValue + approvedValue);
    }

    render() {
        return (
            <div className="canvas-container">
                <canvas ref={this.canvasRef} width={this.state.canvasWidth} height={this.state.canvasHeight}/>
            </div>
            
        );
    }
}