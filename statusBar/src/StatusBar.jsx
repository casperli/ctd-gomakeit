import { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";

import { StatusBarCanvas } from "./components/StatusBarCanvas";
import "./ui/StatusBar.css";

class StatusBar extends Component {
    render() {
        return <StatusBarCanvas 
        draft={this.props.draftValue} draftColor={this.props.draftColor} draftTextColor={this.props.draftTextColor}
        pending={this.props.pendingValue} pendingColor={this.props.pendingColor} pendingTextColor={this.props.pendingTextColor}
        approved={this.props.approvedValue} approvedColor={this.props.approvedColor} approvedTextColor={this.props.approvedTextColor} />;
    }
}

export default hot(StatusBar);
