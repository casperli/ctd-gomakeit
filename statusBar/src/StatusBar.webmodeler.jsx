import { Component, createElement } from "react";
import { StatusBarCanvas } from "./components/StatusBarCanvas";

export class preview extends Component {
    render() {
        return <StatusBarCanvas 
            draft={this.props.draftValue} draftColor={this.props.draftColor} draftTextColor={this.props.draftTextColor}
            pending={this.props.pendingValue} pendingColor={this.props.pendingColor} pendingTextColor={this.props.pendingTextColor}
            approved={this.props.approvedValue} approvedColor={this.props.approvedColor} approvedTextColor={this.props.approvedTextColor} />;
    }
}

export function getPreviewCss() {
    return require("./ui/StatusBar.css");
}
