## StatusBar
[The statusbar to indicate the status of the expenses]

## Features
[feature highlights]

## Usage
[step by step instructions]

## Demo project
[link to sandbox]

## Issues, suggestions and feature requests
[link to GitHub issues]

## Development and contribution
[specify contribute]
