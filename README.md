# CTD Roadshow - Go make it with Mendix

Throughout our Roadshow, we will develop an application for handling expensed - web, mobile and connected to existing backend systems. 3 different styles of software development are involved 
through this roadshow: Developing the basic application with Mendix Studio Pro, extending the application with custom made, React based HTML/JavaScript and individual backend functionality with Java.
To set up the environment for the course, follow the steps in this Readme.

## Initial setup

1. Download and install [git](https://git-scm.com/downloads).
2. Clone this repository.

## Ready for development - Mendix

1. Make sure you have **Windows machine** (at least a VM, if you're using a Mac)
2. Register yourself at Mendix with your **Z�hlke e-mail adress**: [Signup](https://signup.mendix.com/link/signup/?source=direct)
3. [Download](https://appstore.home.mendix.com/link/modelers/) and [install](https://docs.mendix.com/howto/general/install) the latest Mendix Studio Pro (8.1.1)
4. Optional: For further readings, you can find an *introduction course* [here](https://gettingstarted.mendixcloud.com/link/path).

## Ready for development - React & Java

1. Install Visual Studio Code (any other text editor will do it as well)
2. Install [Node.js](https://nodejs.org/en/)
3. No installation for java is needed, all essential parts were installed with the Mendix Studio Pro

## Ready for development - Native Mobile App

1. Install the Mendix **Make It Native App** either for [Android](https://play.google.com/store/apps/details?id=com.mendix.developerapp) or [iOS](https://apps.apple.com/us/app/make-it-native/id1334081181) 

---

## Basic steps